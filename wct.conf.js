module.exports = {
  plugins: {
    local: {
      "browsers": ["chrome", "firefox"],
      seleniumPort: 7055,
      skipSelenium: false, // You have your own selenium server running.
    }
  }
};
